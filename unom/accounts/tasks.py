from celery.decorators import task
from celery.utils.log import get_task_logger
from django.core.mail import EmailMultiAlternatives


from .email import activation_email


logger = get_task_logger(__name__)

@task(name="send_email_activation")

def send_email_activation(site,ctx_dict):

    logger.info("email activation sent")

    return activation_email(site,ctx_dict)
