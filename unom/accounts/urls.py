from django.conf.urls import url, include
from .views import UserRegistrationView,MyActivationView

from .forms import UserRegistrationForm

urlpatterns = [
    url(r'^register/$', UserRegistrationView.as_view(form_class=UserRegistrationForm),
        name='registration_register',),
    url(r'^activate/accounts/(?P<activation_key>\w+)/$', MyActivationView.as_view(),
        name='activation',),

    url(r'^', include('registration.backends.default.urls')),
]
