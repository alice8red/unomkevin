#!!! SEND ACTIVATION EMAIL .

import string
from django.template.loader import render_to_string

from django.core.mail import EmailMultiAlternatives
from django.conf import settings


def activation_email(site,ctx_dict):

            user_email = ctx_dict.get('user')



            activation_email_subject = getattr(settings, 'ACTIVATION_EMAIL_SUBJECT',
                                               'registration/activation_email_subject.txt')
            activation_email_body = getattr(settings, 'ACTIVATION_EMAIL_BODY',
                                            'registration/activation_email.txt')
            activation_email_html = getattr(settings, 'ACTIVATION_EMAIL_HTML',
                                            'registration/activation_email.html')


            subject = (getattr(settings, 'REGISTRATION_EMAIL_SUBJECT_PREFIX', '') +
                       render_to_string(
                           activation_email_subject, ctx_dict))
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            from_email = getattr(settings, 'REGISTRATION_DEFAULT_FROM_EMAIL',
                                 settings.DEFAULT_FROM_EMAIL)
            message_txt = render_to_string(activation_email_body,
                                           ctx_dict)

            email_message = EmailMultiAlternatives(subject, message_txt,
                                                   from_email, [user_email])

    # !!! DELETE THIS AS A CLEAN UP
            if getattr(settings, 'REGISTRATION_EMAIL_HTML', True):
                try:
                    message_html = render_to_string(
                        activation_email_html, ctx_dict, request=request)
                except TemplateDoesNotExist:
                    pass
                else:
                    email_message.attach_alternative(message_html, 'text/html')


            email_message.send()
