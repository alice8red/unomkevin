from django import forms
from django.utils.translation import ugettext_lazy as _
from registration.forms import RegistrationForm
from custom.models import User


class UserRegistrationForm(RegistrationForm):
    first_name = forms.CharField(max_length=30, label=_("First name"))
    last_name = forms.CharField(max_length=30, label=_("Last name"))

    class Meta:
        model = User
        fields = ("first_name", "last_name","email" )

