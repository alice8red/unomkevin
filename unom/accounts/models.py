
#!!! NOTE THAT THE VIEW IN THIS APP WAS ALTERED TO USE THIS MODEL, THIS WOULD INHERIT THE REDUX METHODS FOR SENDING AN ACTIVATION EMAIL
# HOWEVER I WAS UNABLE TO PASS THE REQUIRED VALUES TO THE FUNCTION , It would be best if


from __future__ import unicode_literals

import datetime
import hashlib
import re
import string

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.exceptions import ImproperlyConfigured
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.db import models, transaction
from django.template import TemplateDoesNotExist
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.encoding import python_2_unicode_compatible
from django.utils.timezone import now as datetime_now
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.shortcuts import get_current_site
from .tasks import send_email_activation
from django.core import serializers
from registration.users import UserModel, UserModelString
from registration.models import *

import json

SHA1_RE = re.compile('^[a-f0-9]{40}$')


class MyRegistrationManager(RegistrationManager):

    def mock():
        pass


#!!! NOTE that  using the model here to collect registrations may break other features in registration-
#redux, this was done to demonstrate functionality as required by the task. If time then whole of redux #dependancies should be inherited
# this was done so that the activation email could be handled with celery.

class MyRegistrationProfile(RegistrationProfile):


        objects = MyRegistrationManager()

        def send_activation_email(self,site,request=None):

                site = site.domain
                user = self.user
                user_email= user.email

                ctx_dict = {
            'user': user_email,
            'activation_key': self.activation_key,
            'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
            'site': site,
               }




                print("$$$$$$$$$",site)
                send_email_activation.delay(site,ctx_dict)
