from django.shortcuts import render

from django.contrib.sites.shortcuts import get_current_site
from registration.backends.default.views import RegistrationView, ActivationView
from .models import MyRegistrationProfile as RegistrationProfile
from registration import signals
from .forms import UserRegistrationForm
from custom.models import User


class UserRegistrationView(RegistrationView):

    registration_profile = RegistrationProfile
    form_class = UserRegistrationForm
    template_name = "register.html"


    success_url = 'registration_complete'

    def register(self, form):
        print("*****")
        request = self.request
        site = get_current_site(request)

        new_user_instance = User.objects.create_user(**form.cleaned_data)

        new_user = self.registration_profile.objects.create_inactive_user(
            new_user=new_user_instance,
            site=site,
            send_email=self.SEND_ACTIVATION_EMAIL,
            request=self.request,
        )

        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=self.request)

        return new_user






class MyActivationView(ActivationView):

    registration_profile = RegistrationProfile


    #*** NB SYC USER 2 ZENDECKS
    def get_success_url(self, user):
        return ('zendesk:zendesksyc', (), {})
