$(function() {

load_todos()


function load_todos() {


  $.ajax({
      url : "/todolist/gettodo/",
      type : "GET",


    success : function(json) {

            $('#todo_post_form').val('');
            var x = "hello"
            console.log(x)
            for (var i = 0; i < json.length; i++) {
//*** NO IDEA WhY THIS HAS NOT WORKED, EVEN THE GUYS ON STACK OVERFLOW SAID IT SHOULD BE DONE THIS WAY
//** This has been tested in the console and works with the data given, maybe the object needs converting???
              console.log("PROCESS*")
              $("#posts").prepend( "<div >" +"<h2 class='bg-info text-align-center'> Tasks" +"</h2>"
                                   + "<strong class='btn btn-success'> Name: " + " "  + json[i]['fields'].name + "</strong>"
                                   + "<strong class='btn btn-default'> Task: " + " "  + json[i]['fields'].discription + "</strong>"
                                   + "<strong class='btn btn-default'> Due Date: " + " "  + json[i]['fields'].dueDate + "</strong>"
                                   + "<strong class='btn btn-warning'> Due in: " + " "  + json[i]['fields'].dueMinutes +"<strong> Mins "+"</strong>"+ "</strong>"
                                   + "<strongclass='btn btn-default'> TimeZone: " + " "  + json[i]['fields'].timezoneChoice + "</strong>"
                                   + "<hr>" +"</div>")
                          }
                        },

    error : function(xhr,errmsg,err) {


    alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

          }
});
};




// HANDLES ToDo INPUT


$('#todo_post_form').on('submit', function(event){
                  event.preventDefault();
                  console.log("form submitted!")  // sanity check
                  create_post();
});


// AJAX for posting
function create_post() {
        console.log("create post is working!")


        $.ajax({
            url : "/todolist/posttodo/",
            type : "POST",
            data : { name_field : $('#id_name').val() ,
                    discription_field :  $('#id_discription').val() ,
                    id_duedate :$("#id_duedate").find("input").val(),
                    timezonechoice_field:$('#id_timezoneChoice option:selected').text()
                },

          success : function(json) {

                  $('#todo_post_form').val('');

                  $("#posts").prepend( "<div >" +"<h2 class='bg-info text-align-center'> Tasks" +"</h2>"
                                       + "<strong class='btn btn-success'> Name: " + " "  + json.name + "</strong>"
                                       + "<strong class='btn btn-default'> Task: " + " "  + json.discription + "</strong>"
                                       + "<strong class='btn btn-default'> Due Date: " + " "  + json.dueDate + "</strong>"
                                       + "<strong class='btn btn-warning'> Due in : " + " "  + json.dueMinutes +"<strong> Mins "+"</strong>"+"</strong>"
                                       + "<strong class='btn btn-default'> TimeZone: " + " "  + json.timezoneChoice + "</strong>"
                                       + "<hr>" +"</div>")
                              },

          error : function(xhr,errmsg,err) {


          alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

                }
});
};


// NOTE the CSRF Handling code below was taken from Django Docs directly.

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


});
