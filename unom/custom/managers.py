from django.contrib.auth.base_user import BaseUserManager

class UserManager(BaseUserManager):
    use_in_migrations = True
    #!!! passing of both passwords is a bit of a hack/sticky-tap
    def create_user(self, first_name,last_name,email, password1=None,password2=None,password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )


        # this is set up due to the super user parameters
        if password!=None:
            password2 =password



        user.set_password(password2)
        user.save(using=self._db)
        return user



    def create_superuser(self,first_name,last_name, email, password):
        """
        Creates and saves a superuser with the given email
        """
        user = self.create_user(
                email=email,
                password=password,
                first_name=first_name,
                last_name=last_name,

            )

        user.is_admin = True
        user.is_active =True
        user.is_superuser =True
        user.save(using=self._db)

        return user
