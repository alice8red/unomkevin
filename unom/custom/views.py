from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login
from .models import User
#!!! This code is a sourced from http://stackoverflow.com/questions/13440298/django-how-to-use-built-in-login-view-with-email-instead-of-username
#!!! I made some modifications but largely it is a copy paste
#!!! Just as a note - Kevin


# create a function to resolve email to username
def get_user(email):
    try:
        return User.objects.get(email=email.lower())
    except User.DoesNotExist:

        return None

# create a view that authenticate user with email
def email_login_view(request):
    email = request.POST['email']
    password = request.POST['password']
    username = get_user(email)
    print('****WHAT',username,email)
    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:


            login(request, user)
            #return HttpResponseRedirect(reverse('home:step2'))
            return redirect('todolist:todo_index')
        else:
            #!!!* if time handle
            # Return a 'disabled account' error message
            return redirect('/')

    else:
        #!!!* if time handle
        # Return an 'invalid login' error message.
            print("THIS ONE")
            return redirect('/')
