from django_celery_beat.models import PeriodicTask, IntervalSchedule
from todolist.tasks import email_reminder

# To intialise the shedualar database // once off task on first server run , uncomment lines is unom.urls to start process on first run

def startup():

    schedule, created = IntervalSchedule.objects.get_or_create(
       every=10,
       period=IntervalSchedule.SECONDS,
    )


    PeriodicTask.objects.create(
        interval=schedule,
        name='email_reminder',
        task='email_reminder',
     )
