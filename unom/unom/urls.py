"""unom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""


from django.conf import settings
from django.conf.urls import *
from django.conf.urls.static import static
from django.contrib import admin

from startup import startup


urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', include('home.urls'), name='home'),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^custom/', include('custom.urls', namespace='custom')),
    url(r'^todolist/', include('todolist.urls',namespace='todolist')),
    url(r'^zendesk/', include('zendesk.urls',namespace='zendesk')),
    url(r'^', include('registration.backends.default.urls')),


]


#*** ON FIRST RUN OF THE SERVER UNCOMMENT THE FOLLOWING STARTUP FUCNTION TO START INTIALISATION OF CELERY django_celery_beat SCHEDUALED TASKS> WHEN CREATED RECOMMENT LINES
# MUST BE A BETTER SOULTIION BUT FOR NOW THIS THE DIRTY FIX

# startup()
