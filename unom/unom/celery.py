from __future__ import absolute_import, unicode_literals
import os

from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'unom.settings')
app = Celery('unom')

# This reads, e.g., CELERY_ACCEPT_CONTENT = ['json'] from settings.py:
app.config_from_object('django.conf:settings',namespace='Celery')

# # For autodiscover_tasks to work, you must define your tasks in a file called 'tasks.py'.
# app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.autodiscover_tasks()


if __name__ == '__main__':
    app.start()


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))
