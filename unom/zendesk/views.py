from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from custom.models import User

from zenapi import *


# Sync the User to Zendesk after user activation , as found in accounts app
def zendeskSyc(request):

    user =  user = User.objects.get(email=request.user)

    email = user.email

    name = user.get_full_name()

    sycUserInfo(email=email,name=name)

    return redirect('todolist:todo_index')
