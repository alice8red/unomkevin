
import datetime
import pytz
import time
import re

def dueMin(duedate,timezone):

    epoch = datetime.datetime(2016, 11, 19, 00, 00, 00, 00, pytz.UTC)


    yr, mon, d, h, mins, s = stripValues(duedate)

    tz = pytz.timezone(timezone)

    dueDateOffset= datetime.datetime(yr,mon,d,h,mins,s,00)
    timeUTC = datetime.datetime(yr,mon,d,h,mins,s,00,pytz.UTC)

    tz = pytz.timezone(timezone)
    offset_seconds = tz.utcoffset(dueDateOffset).seconds

    deltaSeconds = ( timeUTC - epoch).total_seconds()

    offset = deltaSeconds - offset_seconds

    minutes2alert = int(offset/60)

    print("$$$$",minutes2alert)

    return minutes2alert



#!!! Worst code I have ever written, note to self ... python 3 date functions win
def stripValues( string, maxsplit=0):

    delimiters = "-", ":", " "

    regexPattern = '|'.join(map(re.escape, delimiters))

    keys = ['year','month','day','hrs','mins','secs']

    values =re.split(regexPattern, string, maxsplit)

    dictionary = dict(zip(keys, values))

    yr = int(dictionary.get("year", None))
    mon = int(dictionary.get("month", None))
    d = int(dictionary.get("day", None))
    h = int(dictionary.get("hrs", None))
    mins = int(dictionary.get("mins", None))
    s = int(dictionary.get("secs", None))

    return yr, mon ,d,h,mins,s
