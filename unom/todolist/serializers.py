from custom.models import User

from rest_framework import serializers
from .models import ToDoList


class ToDoSerializer(serializers.ModelSerializer):
#!!!? dont understand the use of the 'id' in this field, is it the primary key of ToDoList, why is it needed?
#!!!? for the user SlugRelatedField, why does it query objects.all(), does this mean it gets all users,
# what is happening here in the backend (read - look at rest_framework code base in future)

    def __init__(self,user_id,data=None):

        self.user_id = user_id
        # self.data =data

        user = serializers.SlugRelatedField(
            queryset=User.objects.get(email=user_id), slug_field='username'
            )


    class Meta:
        model = ToDoList
        fields = ('user','name', 'discription', 'dueDate','timezoneChoice','dueMinutes')
