from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.core import serializers

from custom.models import User

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import ToDoSerializer
from .models import ToDoList
from .forms import toDoForm
import json

from duemins import *

from emailsender import *

#***? Research how to handle class based views with serializers


@login_required
def todo(request):

    form =toDoForm()
    tmpl_vars = {'form': form}


    return render(request, 'todo/index.html', tmpl_vars)



def todo_all(request):

    if request.method == 'GET':


         user = User.objects.get(email=request.user)

         todos = ToDoList.objects.filter(user=user).order_by('dueMinutes')

         data = serializers.serialize('json', todos)


         return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )


#**** NOTE the dueDate in the Model is recieving a naive datetime, this shows up as a warning inthe console as not being timezone aware,
# this is correct with the dynamics and processing of this app, it requires an naive date time which is processed for todos to be sent which are agnostic
# For fusture developement a new field in the model set which captures an aware datetime inorder for further functionality to be set up
# The current setup allows for many complexities of time zone to be circumvented for reminders to be sent.
# please refence the manager.py in todoslist app for more details

def post_todo(request) :

    if request.method == 'POST':

           user = User.objects.get(email=request.user)

           getDue = request.POST.get('id_duedate')

           timezone = request.POST.get('timezonechoice_field')

           minDue = dueMin(getDue,timezone)

           name = request.POST.get('name_field')

           discription = request.POST.get('discription_field')

           dueMinutes = minDue

           create = ToDoList.objects.create(user  = user,dueDate = getDue,timezoneChoice = timezone,name = name,discription = discription,dueMinutes = dueMinutes)
           create.save()



           data = { 'name': name,
                    'discription': discription,
                    'dueDate': getDue,
                    'timezoneChoice': timezone,
                    'dueMinutes': minDue

                        }


           return HttpResponse(
                json.dumps(data),
                content_type="application/json")
    else:

        return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json")






#
# #*** WORK OUT API IN THE FUTURE, issue with passing the Slugflied .get
# # ISSUE WITH GETTING THE get instead of the all in Sliugfield
#   File "/home/g5/devuno2/env/local/lib/python2.7/site-packages/rest_framework/serializers.py", line 230, in is_valid
#     'Cannot call `.is_valid()` as no `data=` keyword argument was '
# AssertionError: Cannot call `.is_valid()` as no `data=` keyword argument was passed when instantiating the serializer instance.

#
#
# @api_view(['GET', 'POST'])
# def todo_all(request):
#
#     if request.method == 'GET':
#
#         user = User.objects.get(email=request.user)
#
#         todos = ToDoList.objects.filter(user=request.user.id)
#
#         serializer = ToDoSerializer(todos ,many=True)
#
#         return Response(serializer.data)
#
#     elif request.method == 'POST':
#         user = User.objects.get(email=request.user)
#         getDue = request.POST.get('id_duedate')
#         print('YYYYY',getDue)
#
#         timezone = request.data.get('timezonechoice_field')
#         # calculate the mintures due , function found in duemins.py
#         minDue = dueMin(getDue,timezone)
#         print("####",minDue)
#
#
#         data = {'name': request.data.get('name_field'),
#                 'discription': request.data.get('discription_field'),
#                 'dueDate': request.data.get('id_duedate'),
#                 'timezoneChoice': timezone,
#                 'dueMinutes': minDue,
#                 'user':user
#                 }
#
#
#
#         serializer = ToDoSerializer(user,data=data)
#
#         if serializer.is_valid():
#
#             serializer.save()
#
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
