from django import forms
import datetime
# from django.forms.extras.widgets import SelectDateWidget
from datetimewidget.widgets import DateTimeWidget

from .models import ToDoList


class toDoForm(forms.ModelForm):

    class Meta:

        model = ToDoList

        fields = ('name','dueDate','timezoneChoice','discription')
        dueDate = widgets = {
            #Use localization and bootstrap 3
            'dueDate': DateTimeWidget(attrs={'id':"id_duedate"}, usel10n = True, bootstrap_version=3)
        }


    discription = forms.TextInput()
