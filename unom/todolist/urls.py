from django.conf.urls import url, include
from . import views

urlpatterns = [

    url(r'^$', views.todo, name='todo_index'),

    url(r'^posttodo/$', views.post_todo, name='post_todo'),
    url(r'^gettodo/$', views.todo_all, name='todo_all'),


    # api
    # url(r'^api/v1/todos/$',views.todo_all ,name= 'todo_all'),


]
