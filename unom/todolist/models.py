from __future__ import unicode_literals
from django.db import models
from .managers import EmailSchedular
from django.conf import settings

import pytz


class ToDoList (models.Model):

    #create timezone choices
    a = pytz.common_timezones
    b = pytz.common_timezones

    TIME_ZONE_CHOICES = tuple(zip(a,b))

    timezoneChoice = models.CharField(max_length=250, choices=TIME_ZONE_CHOICES,default='Africa/Johannesburg')

    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)



    name = models.CharField(max_length = 100, unique = False)
    discription = models.TextField(max_length = 500)

    #Note this is used in the celery email scheduler and is part of the queryset mechanism
    # to find the todos to send to users, due to this apps mechanism, it is not required that the user
    dueMinutes = models.IntegerField(default=0, blank=False, null=True)
    emailSent = models.BooleanField(default=False)

    #!!!$ The duedate is set to display the duedate to the user on the frontend,
    # whether a query will result in displaying the users current timezone is unknown at this point
    # this is left mute until future research can be done

    dueDate = models.DateTimeField()
    create_at = models.DateTimeField(auto_now_add=True)

    #*** FOR FUTURE dueDate = model.DateTimeField()
    #*** FOR FUTURE timezone = models.CharField(max_length = 100, unique = False)=

    objects = EmailSchedular()


    class Meta:
        ordering = ['create_at']
