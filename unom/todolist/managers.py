from django.contrib.auth.base_user import BaseUserManager

import datetime
import pytz


from django.db import models

class TodoQuerySet(models.query.QuerySet):

    # def __init__(self,a,using):
    #
    #     #*** future: get notice period past as a vlue to this class so that one can easly change it
    #     #*** CHANGE THE NOTICE PERIOD AFTER TESTING




    #all past todos , which includes those which will be due by the offset in the fucture

    def toDodue (self):

        self.notice_period = 1

        self.currentTime = datetime.datetime.now(pytz.timezone('UCT'))

        self.epoch = datetime.datetime(2016, 11, 19, 00, 00, 00, 00, pytz.UTC)

        self.deltaMintutes = int(((self.currentTime-self.epoch).total_seconds())/60)

        self.offset = self.deltaMintutes - self.notice_period


        return self.filter(dueMinutes__lte=self.offset)

    # todos that have not been emailed yet
    def emailSent(self):
        return self.filter(emailSent=False)


class EmailSchedular(BaseUserManager):
# """
# The manager creates the query to get todolist objects which are due,
# This is done by looking at minutes past since the epoch(2016,11,17,00,00,00) UCT.
# Instead of dealing with timezones, seconds are recorded until the task is due. This is commpared
# with the delta of the current time to epoch. If the seconds are greater then the object is not
# returned , if it is less then it is. NOTE that future seconds are offset by a notice period and this
# is hence subtracted datetime.now().
# Note that incase of a sytem crash or lag and to prevent all past results resending , the
# filter checks for the truth value of "email_sent"
#
# """

    def get_queryset(self):

        return TodoQuerySet(self.model, using=self._db)

    def toDodue (self):

        return self.get_queryset().toDodue()

    def emailSent(self):

        return self.get_queryset().emailSent()
