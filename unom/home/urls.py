from django.conf.urls import url, include

from accounts.views import UserRegistrationView
from accounts.forms import UserRegistrationForm


urlpatterns = [
    url(r'^$', UserRegistrationView.as_view(), name='index'),
]

